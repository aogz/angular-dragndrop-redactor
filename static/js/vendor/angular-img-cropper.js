(function() {
var crop = angular.module('ngImgCrop', []);

crop.factory('cropAreaRectangle', ['cropArea', function (CropArea) {
    var CropAreaRectangle = function () {
        CropArea.apply(this, arguments);

        this._resizeCtrlBaseRadius = 15;
        this._resizeCtrlNormalRatio = 0.75;
        this._resizeCtrlHoverRatio = 1;
        this._iconMoveNormalRatio = 0.9;
        this._iconMoveHoverRatio = 1.2;

        this._resizeCtrlNormalRadius = this._resizeCtrlBaseRadius * this._resizeCtrlNormalRatio;
        this._resizeCtrlHoverRadius = this._resizeCtrlBaseRadius * this._resizeCtrlHoverRatio;

        this._posDragStartX = 0;
        this._posDragStartY = 0;
        this._posResizeStartX = 0;
        this._posResizeStartY = 0;
        this._posResizeStartSize = {
            w: 0,
            h: 0
        };

        this._resizeCtrlIsHover = -1;
        this._areaIsHover = false;
        this._resizeCtrlIsDragging = -1;
        this._areaIsDragging = false;
    };

    CropAreaRectangle.prototype = new CropArea();

    // return a type string
    CropAreaRectangle.prototype.getType = function () {
        return 'rectangle';
    };

    CropAreaRectangle.prototype._calcRectangleCorners = function () {
        var size = this.getSize();
        var se = this.getSouthEastBound();
        return [
            [size.x, size.y], //northwest
            [se.x, size.y], //northeast
            [size.x, se.y], //southwest
            [se.x, se.y] //southeast
        ];
    };

    CropAreaRectangle.prototype._calcRectangleDimensions = function () {
        var size = this.getSize();
        var se = this.getSouthEastBound();
        return {
            left: size.x,
            top: size.y,
            right: se.x,
            bottom: se.y
        };
    };

    CropAreaRectangle.prototype._isCoordWithinArea = function (coord) {
        var rectangleDimensions = this._calcRectangleDimensions();
        return (coord[0] >= rectangleDimensions.left && coord[0] <= rectangleDimensions.right && coord[1] >= rectangleDimensions.top && coord[1] <= rectangleDimensions.bottom);
    };

    CropAreaRectangle.prototype._isCoordWithinResizeCtrl = function (coord) {
        var resizeIconsCenterCoords = this._calcRectangleCorners();
        var res = -1;
        for (var i = 0, len = resizeIconsCenterCoords.length; i < len; i++) {
            var resizeIconCenterCoords = resizeIconsCenterCoords[i];
            if (coord[0] > resizeIconCenterCoords[0] - this._resizeCtrlHoverRadius && coord[0] < resizeIconCenterCoords[0] + this._resizeCtrlHoverRadius &&
                coord[1] > resizeIconCenterCoords[1] - this._resizeCtrlHoverRadius && coord[1] < resizeIconCenterCoords[1] + this._resizeCtrlHoverRadius) {
                res = i;
                break;
            }
        }
        return res;
    };

    CropAreaRectangle.prototype._drawArea = function (ctx, center, size) {
        ctx.rect(size.x, size.y, size.w, size.h);
    };

    CropAreaRectangle.prototype.draw = function () {
        CropArea.prototype.draw.apply(this, arguments);

        var center = this.getCenterPoint();
        // draw move icon
        this._cropCanvas.drawIconMove([center.x, center.y], this._areaIsHover ? this._iconMoveHoverRatio : this._iconMoveNormalRatio);

        // draw resize thumbs
        var resizeIconsCenterCoords = this._calcRectangleCorners();
        for (var i = 0, len = resizeIconsCenterCoords.length; i < len; i++) {
            var resizeIconCenterCoords = resizeIconsCenterCoords[i];
            this._cropCanvas.drawIconResizeCircle(resizeIconCenterCoords, this._resizeCtrlBaseRadius, this._resizeCtrlIsHover === i ? this._resizeCtrlHoverRatio : this._resizeCtrlNormalRatio);
        }
    };

    CropAreaRectangle.prototype.processMouseMove = function (mouseCurX, mouseCurY) {
        var cursor = 'default';
        var res = false;

        this._resizeCtrlIsHover = -1;
        this._areaIsHover = false;

        if (this._areaIsDragging) {
            this.setCenterPointOnMove({
                x: mouseCurX - this._posDragStartX,
                y: mouseCurY - this._posDragStartY
            });
            this._areaIsHover = true;
            cursor = 'move';
            res = true;
            this._events.trigger('area-move');
        } else if (this._resizeCtrlIsDragging > -1) {
            var s = this.getSize();
            var se = this.getSouthEastBound();
            var posX = mouseCurX;
            switch (this._resizeCtrlIsDragging) {
                case 0: // Top Left
                    if (this._aspect) posX = se.x - ((se.y - mouseCurY) * this._aspect);
                    this.setSizeByCorners({
                        x: posX,
                        y: mouseCurY
                    }, {
                        x: se.x,
                        y: se.y
                    });
                    cursor = 'nwse-resize';
                    break;
                case 1: // Top Right
                    if (this._aspect) posX = s.x + ((se.y - mouseCurY) * this._aspect);
                    this.setSizeByCorners({
                        x: s.x,
                        y: mouseCurY
                    }, {
                        x: posX,
                        y: se.y
                    });
                    cursor = 'nesw-resize';
                    break;
                case 2: // Bottom Left
                    if (this._aspect) posX = se.x - ((mouseCurY - s.y) * this._aspect);
                    this.setSizeByCorners({
                        x: posX,
                        y: s.y
                    }, {
                        x: se.x,
                        y: mouseCurY
                    });
                    cursor = 'nesw-resize';
                    break;
                case 3: // Bottom Right
                    if (this._aspect) posX = s.x + ((mouseCurY - s.y) * this._aspect);
                    this.setSizeByCorners({
                        x: s.x,
                        y: s.y
                    }, {
                        x: posX,
                        y: mouseCurY
                    });
                    cursor = 'nwse-resize';
                    break;
            }

            this._resizeCtrlIsHover = this._resizeCtrlIsDragging;
            res = true;
            this._events.trigger('area-resize');
        } else {
            var hoveredResizeBox = this._isCoordWithinResizeCtrl([mouseCurX, mouseCurY]);
            if (hoveredResizeBox > -1) {
                switch (hoveredResizeBox) {
                    case 0:
                        cursor = 'nwse-resize';
                        break;
                    case 1:
                        cursor = 'nesw-resize';
                        break;
                    case 2:
                        cursor = 'nesw-resize';
                        break;
                    case 3:
                        cursor = 'nwse-resize';
                        break;
                }
                this._areaIsHover = false;
                this._resizeCtrlIsHover = hoveredResizeBox;
                res = true;
            } else if (this._isCoordWithinArea([mouseCurX, mouseCurY])) {
                cursor = 'move';
                this._areaIsHover = true;
                res = true;
            }
        }

        angular.element(this._ctx.canvas).css({
            'cursor': cursor
        });

        return res;
    };

    CropAreaRectangle.prototype.processMouseDown = function (mouseDownX, mouseDownY) {
        var isWithinResizeCtrl = this._isCoordWithinResizeCtrl([mouseDownX, mouseDownY]);
        if (isWithinResizeCtrl > -1) {
            this._areaIsDragging = false;
            this._areaIsHover = false;
            this._resizeCtrlIsDragging = isWithinResizeCtrl;
            this._resizeCtrlIsHover = isWithinResizeCtrl;
            this._posResizeStartX = mouseDownX;
            this._posResizeStartY = mouseDownY;
            this._posResizeStartSize = this._size;
            this._events.trigger('area-resize-start');
        } else if (this._isCoordWithinArea([mouseDownX, mouseDownY])) {
            this._areaIsDragging = true;
            this._areaIsHover = true;
            this._resizeCtrlIsDragging = -1;
            this._resizeCtrlIsHover = -1;
            var center = this.getCenterPoint();
            this._posDragStartX = mouseDownX - center.x;
            this._posDragStartY = mouseDownY - center.y;
            this._events.trigger('area-move-start');
        }
    };

    CropAreaRectangle.prototype.processMouseUp = function (/*mouseUpX, mouseUpY*/) {
        if (this._areaIsDragging) {
            this._areaIsDragging = false;
            this._events.trigger('area-move-end');
        }
        if (this._resizeCtrlIsDragging > -1) {
            this._resizeCtrlIsDragging = -1;
            this._events.trigger('area-resize-end');
        }
        this._areaIsHover = false;
        this._resizeCtrlIsHover = -1;

        this._posDragStartX = 0;
        this._posDragStartY = 0;
    };

    return CropAreaRectangle;
}]);

crop.factory('cropArea', ['cropCanvas', function(CropCanvas) {
    var CropArea = function(ctx, events) {
        this._ctx = ctx;
        this._events = events;

        this._minSize = {
            x: 0,
            y: 0,
            w: 80,
            h: 80
        };

        this._initSize = undefined;
        this._allowCropResizeOnCorners = false;

        this._forceAspectRatio = false;
        this._aspect = null;

        this._cropCanvas = new CropCanvas(ctx);

        this._image = new Image();
        this._size = {
            x: 0,
            y: 0,
            w: 150,
            h: 150
        };
    };

    /* GETTERS/SETTERS */

    CropArea.prototype.setAllowCropResizeOnCorners = function(bool) {
        this._allowCropResizeOnCorners=bool;
    };
    CropArea.prototype.getImage = function() {
        return this._image;
    };
    CropArea.prototype.setImage = function(image) {
        this._image = image;
    };

    CropArea.prototype.setForceAspectRatio = function(force) {
        this._forceAspectRatio = force;
    };

    CropArea.prototype.setAspect = function(aspect) {
        this._aspect=aspect;
    };

    CropArea.prototype.getAspect = function() {
        return this._aspect;
    };

    CropArea.prototype.getCanvasSize = function() {
        return {
          w: this._ctx.canvas.width,
          h: this._ctx.canvas.height
        };
    };

    CropArea.prototype.getSize = function() {
        return this._size;
    };

    CropArea.prototype.setSize = function(size) {
        size = this._processSize(size);
        this._size = this._preventBoundaryCollision(size);
    };

    CropArea.prototype.setSizeOnMove = function(size) {
        size = this._processSize(size);
        if(this._allowCropResizeOnCorners) this._size = this._preventBoundaryCollision(size);
        else this._size = this._allowMouseOutsideCanvas(size);
    };

    CropArea.prototype.setSizeByCorners = function(northWestCorner, southEastCorner) {

        var size = {
            x: northWestCorner.x,
            y: northWestCorner.y,
            w: southEastCorner.x - northWestCorner.x,
            h: southEastCorner.y - northWestCorner.y
        };
        this.setSize(size);
    };

    CropArea.prototype.getSouthEastBound = function() {
        return this._southEastBound(this.getSize());
    };

    CropArea.prototype.setMinSize = function(size) {
        this._minSize = this._processSize(size);
        this.setSize(this._minSize);
    };

    CropArea.prototype.getMinSize = function() {
        return this._minSize;
    };

    CropArea.prototype.getCenterPoint = function() {
        var s = this.getSize();
        return {
            x: s.x + (s.w / 2),
            y: s.y + (s.h / 2)
        };
    };

    CropArea.prototype.setCenterPoint = function(point) {
        var s = this.getSize();
        this.setSize({
            x: point.x - s.w / 2,
            y: point.y - s.h / 2,
            w: s.w,
            h: s.h
        });
    };
    
    CropArea.prototype.setCenterPointOnMove = function(point) {
        var s = this.getSize();
        this.setSizeOnMove({
            x: point.x - s.w / 2,
            y: point.y - s.h / 2,
            w: s.w,
            h: s.h
        });
    };

    CropArea.prototype.setInitSize = function(size) {
        this._initSize = this._processSize(size);
        this.setSize(this._initSize);
    };

    CropArea.prototype.getInitSize = function() {
        return this._initSize;
    };

    // return a type string
    CropArea.prototype.getType = function() {
        //default to circle
        return 'circle';
    };

    /* FUNCTIONS */
    CropArea.prototype._allowMouseOutsideCanvas = function(size) {
        var canvasH = this._ctx.canvas.height,
            canvasW = this._ctx.canvas.width;
        var newSize = {
            w: size.w,
            h: size.h,
        };
        if(size.x<0) newSize.x=0;
        else if(size.x+size.w>canvasW) newSize.x=canvasW-size.w;
        else newSize.x=size.x;
        if(size.y<0) newSize.y=0;
        else if(size.y+size.h>canvasH) newSize.y=canvasH-size.h;
        else newSize.y=size.y;
        return newSize;
    };
    CropArea.prototype._preventBoundaryCollision = function(size) {
        var canvasH = this._ctx.canvas.height,
            canvasW = this._ctx.canvas.width;

        var nw = {
            x: size.x,
            y: size.y
        };
        var se = this._southEastBound(size);

        // check northwest corner
        if (nw.x < 0) {
            nw.x = 0;
        }
        if (nw.y < 0) {
            nw.y = 0;
        }

        // check southeast corner
        if (se.x > canvasW) {
            se.x = canvasW
        }
        if (se.y > canvasH) {
            se.y = canvasH
        }

        var newSizeWidth = (this._forceAspectRatio) ? size.w : se.x - nw.x,
            newSizeHeight = (this._forceAspectRatio) ? size.h : se.y - nw.y;

        // save rectangle scale
        if(this._aspect){
            newSizeWidth = newSizeHeight * this._aspect;
            if(nw.x+newSizeWidth>canvasW){
                newSizeWidth=canvasW-nw.x;
                newSizeHeight=newSizeWidth/this._aspect;
                if(this._minSize.w>newSizeWidth) newSizeWidth=this._minSize.w;
                if(this._minSize.h>newSizeHeight) newSizeHeight=this._minSize.h;
                nw.x=canvasW-newSizeWidth;
            }
            if(nw.y+newSizeHeight>canvasH) nw.y=canvasH-newSizeHeight;
        }

        // save square scale
        if(this._forceAspectRatio) {
            newSizeWidth = newSizeHeight;
            if(nw.x+newSizeWidth>canvasW){
                newSizeWidth=canvasW-nw.x;
                if(newSizeWidth<this._minSize.w) newSizeWidth=this._minSize.w;
                newSizeHeight=newSizeWidth;
            }
        }

        var newSize = {
            x: nw.x,
            y: nw.y,
            w: newSizeWidth,
            h: newSizeHeight
        };

        //check size (if < min, adjust nw corner)
        if ( (newSize.w < this._minSize.w) && !this._forceAspectRatio) {
            newSize.w = this._minSize.w;
            se = this._southEastBound(newSize);
            //adjust se corner, if it's out of bounds
            if (se.x > canvasW) {
                se.x = canvasW;
                //adjust nw corner according to min width
                nw.x = Math.max(se.x - canvasW, se.x - this._minSize.w);
                newSize = {
                    x: nw.x,
                    y: nw.y,
                    w: se.x - nw.x,
                    h: se.y - nw.y
                };
            }
        }

        if ( (newSize.h < this._minSize.h) && !this._forceAspectRatio) {
            newSize.h = this._minSize.h;
            se = this._southEastBound(newSize);

            if (se.y > canvasH) {
                se.y = canvasH;
                //adjust nw corner according to min height
                nw.y = Math.max(se.y - canvasH, se.y - this._minSize.h);
                newSize = {
                    x: nw.x,
                    y: nw.y,
                    w: se.x - nw.x,
                    h: se.y - nw.y
                };
            }
        }

        if(this._forceAspectRatio) {
            //check if outside SE bound
            se = this._southEastBound(newSize);
            if (se.y > canvasH) {
                newSize.y = canvasH - newSize.h;
            }
            if (se.x > canvasW) {
                newSize.x = canvasW - newSize.w;
            }
        }

        return newSize;
    };

    CropArea.prototype._dontDragOutside = function() {
        var h = this._ctx.canvas.height,
            w = this._ctx.canvas.width;

        if (this._width > w) {
            this._width = w;
        }
        if (this._height > h) {
            this._height = h;
        }
        if (this._x < this._width / 2) {
            this._x = this._width / 2;
        }
        if (this._x > w - this._width / 2) {
            this._x = w - this._width / 2;
        }
        if (this._y < this._height / 2) {
            this._y = this._height / 2;
        }
        if (this._y > h - this._height / 2) {
            this._y = h - this._height / 2;
        }
    };

    CropArea.prototype._drawArea = function() {};

    CropArea.prototype._processSize = function(size) {
        // make this polymorphic to accept a single floating point number
        // for square-like sizes (including circle)
        if (typeof size == "number") {
            size = {
                w: size,
                h: size
            };
        }
        var width = size.w;
        if(this._aspect) width = size.h * this._aspect;
        return {
            x: size.x || this.getSize().x,
            y: size.y || this.getSize().y,
            w: width || this._minSize.w,
            h: size.h || this._minSize.h
        };
    };

    CropArea.prototype._southEastBound = function(size) {
        return {
            x: size.x + size.w,
            y: size.y + size.h
        };
    };

    CropArea.prototype.draw = function() {
        // draw crop area
        this._cropCanvas.drawCropArea(this._image, this.getCenterPoint(), this._size, this._drawArea);
    };

    CropArea.prototype.processMouseMove = function() {};

    CropArea.prototype.processMouseDown = function() {};

    CropArea.prototype.processMouseUp = function() {};

    return CropArea;
}]);

crop.factory('cropCanvas', [function() {
    // Shape = Array of [x,y]; [0, 0] - center
    var shapeArrowNW = [
        [-0.5, -2],
        [-3, -4.5],
        [-0.5, -7],
        [-7, -7],
        [-7, -0.5],
        [-4.5, -3],
        [-2, -0.5]
    ];
    var shapeArrowNE = [
        [0.5, -2],
        [3, -4.5],
        [0.5, -7],
        [7, -7],
        [7, -0.5],
        [4.5, -3],
        [2, -0.5]
    ];
    var shapeArrowSW = [
        [-0.5, 2],
        [-3, 4.5],
        [-0.5, 7],
        [-7, 7],
        [-7, 0.5],
        [-4.5, 3],
        [-2, 0.5]
    ];
    var shapeArrowSE = [
        [0.5, 2],
        [3, 4.5],
        [0.5, 7],
        [7, 7],
        [7, 0.5],
        [4.5, 3],
        [2, 0.5]
    ];
    var shapeArrowN = [
        [-1.5, -2.5],
        [-1.5, -6],
        [-5, -6],
        [0, -11],
        [5, -6],
        [1.5, -6],
        [1.5, -2.5]
    ];
    var shapeArrowW = [
        [-2.5, -1.5],
        [-6, -1.5],
        [-6, -5],
        [-11, 0],
        [-6, 5],
        [-6, 1.5],
        [-2.5, 1.5]
    ];
    var shapeArrowS = [
        [-1.5, 2.5],
        [-1.5, 6],
        [-5, 6],
        [0, 11],
        [5, 6],
        [1.5, 6],
        [1.5, 2.5]
    ];
    var shapeArrowE = [
        [2.5, -1.5],
        [6, -1.5],
        [6, -5],
        [11, 0],
        [6, 5],
        [6, 1.5],
        [2.5, 1.5]
    ];

    // Colors
    var colors = {
        areaOutline: '#fff',
        resizeBoxStroke: '#fff',
        resizeBoxFill: '#444',
        resizeBoxArrowFill: '#fff',
        resizeCircleStroke: '#fff',
        resizeCircleFill: '#444',
        moveIconFill: '#fff'
    };

    return function(ctx) {

        /* Base functions */

        // Calculate Point
        var calcPoint = function(point, offset, scale) {
            return [scale * point[0] + offset[0], scale * point[1] + offset[1]];
        };

        // Draw Filled Polygon
        var drawFilledPolygon = function(shape, fillStyle, centerCoords, scale) {
            ctx.save();
            ctx.fillStyle = fillStyle;
            ctx.beginPath();
            var pc, pc0 = calcPoint(shape[0], centerCoords, scale);
            ctx.moveTo(pc0[0], pc0[1]);

            for (var p in shape) {
                if (p > 0) {
                    pc = calcPoint(shape[p], centerCoords, scale);
                    ctx.lineTo(pc[0], pc[1]);
                }
            }

            ctx.lineTo(pc0[0], pc0[1]);
            ctx.fill();
            ctx.closePath();
            ctx.restore();
        };

        /* Icons */

        this.drawIconMove = function(centerCoords, scale) {
            drawFilledPolygon(shapeArrowN, colors.moveIconFill, centerCoords, scale);
            drawFilledPolygon(shapeArrowW, colors.moveIconFill, centerCoords, scale);
            drawFilledPolygon(shapeArrowS, colors.moveIconFill, centerCoords, scale);
            drawFilledPolygon(shapeArrowE, colors.moveIconFill, centerCoords, scale);
        };

        this.drawIconResizeCircle = function(centerCoords, circleRadius, scale) {
            var scaledCircleRadius = circleRadius * scale;
            ctx.save();
            ctx.strokeStyle = colors.resizeCircleStroke;
            ctx.lineWidth = 2;
            ctx.fillStyle = colors.resizeCircleFill;
            ctx.beginPath();
            ctx.arc(centerCoords[0], centerCoords[1], scaledCircleRadius, 0, 2 * Math.PI);
            ctx.fill();
            ctx.stroke();
            ctx.closePath();
            ctx.restore();
        };

        this.drawIconResizeBoxBase = function(centerCoords, boxSize, scale) {
            var scaledBoxSize = boxSize * scale;
            ctx.save();
            ctx.strokeStyle = colors.resizeBoxStroke;
            ctx.lineWidth = 2;
            ctx.fillStyle = colors.resizeBoxFill;
            ctx.fillRect(centerCoords[0] - scaledBoxSize / 2, centerCoords[1] - scaledBoxSize / 2, scaledBoxSize, scaledBoxSize);
            ctx.strokeRect(centerCoords[0] - scaledBoxSize / 2, centerCoords[1] - scaledBoxSize / 2, scaledBoxSize, scaledBoxSize);
            ctx.restore();
        };
        this.drawIconResizeBoxNESW = function(centerCoords, boxSize, scale) {
            this.drawIconResizeBoxBase(centerCoords, boxSize, scale);
            drawFilledPolygon(shapeArrowNE, colors.resizeBoxArrowFill, centerCoords, scale);
            drawFilledPolygon(shapeArrowSW, colors.resizeBoxArrowFill, centerCoords, scale);
        };
        this.drawIconResizeBoxNWSE = function(centerCoords, boxSize, scale) {
            this.drawIconResizeBoxBase(centerCoords, boxSize, scale);
            drawFilledPolygon(shapeArrowNW, colors.resizeBoxArrowFill, centerCoords, scale);
            drawFilledPolygon(shapeArrowSE, colors.resizeBoxArrowFill, centerCoords, scale);
        };

        /* Crop Area */

        this.drawCropArea = function(image, centerCoords, size, fnDrawClipPath) {
            var xRatio = Math.abs(image.width / ctx.canvas.width),
                yRatio = Math.abs(image.height / ctx.canvas.height),
                xLeft = Math.abs(centerCoords.x - size.w / 2),
                yTop = Math.abs(centerCoords.y - size.h / 2);

            ctx.save();
            ctx.strokeStyle = colors.areaOutline;
            ctx.lineWidth = 2;
            ctx.beginPath();
            fnDrawClipPath(ctx, centerCoords, size);
            ctx.stroke();
            ctx.clip();

            // draw part of original image
            if (size.w > 0) {
                ctx.drawImage(image, xLeft * xRatio, yTop * yRatio, Math.abs(size.w * xRatio), Math.abs(size.h * yRatio), xLeft, yTop, Math.abs(size.w), Math.abs(size.h));
            }

            ctx.beginPath();
            fnDrawClipPath(ctx, centerCoords, size);
            ctx.stroke();
            ctx.clip();

            ctx.restore();
        };

    };
}]);


crop.factory('cropHost', ['$document', '$q', 'cropAreaRectangle', function($document, $q, CropAreaRectangle) {
    /* STATIC FUNCTIONS */

    // Get Element's Offset
    var getElementOffset = function(elem) {
        var box = elem.getBoundingClientRect();

        var body = document.body;
        var docElem = document.documentElement;

        var scrollTop = window.pageYOffset || docElem.scrollTop || body.scrollTop;
        var scrollLeft = window.pageXOffset || docElem.scrollLeft || body.scrollLeft;

        var clientTop = docElem.clientTop || body.clientTop || 0;
        var clientLeft = docElem.clientLeft || body.clientLeft || 0;

        var top = box.top + scrollTop - clientTop;
        var left = box.left + scrollLeft - clientLeft;

        var colorPaletteLength = 8;

        return {
            top: Math.round(top),
            left: Math.round(left)
        };
    };

    return function(elCanvas, opts, events) {
        /* PRIVATE VARIABLES */

        // Object Pointers
        var ctx = null,
            image = null,
            theArea = null,
            initMax = null,
            isAspectRatio = null,
            self = this,

            // Dimensions
            minCanvasDims = [100, 100],
            maxCanvasDims = [300, 300],

            // Result Image size
            resImgSizeArray = [],
            resImgSize = {
                w: 200,
                h: 200
            },
            areaMinRelativeSize = null,

            // Result Image type
            resImgFormat = 'image/png',

            // Result Image quality
            resImgQuality = null,

            forceAspectRatio = false;

        /* PRIVATE FUNCTIONS */
        this.setInitMax = function(bool){
            initMax=bool;
        }
        this.setAllowCropResizeOnCorners = function(bool){
            theArea.setAllowCropResizeOnCorners(bool);
        }
        // Draw Scene
        function drawScene() {
            // clear canvas
            ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);

            if (image !== null) {
                // draw source image
                ctx.drawImage(image, 0, 0, ctx.canvas.width, ctx.canvas.height);

                ctx.save();

                // and make it darker
                ctx.fillStyle = 'rgba(0, 0, 0, 0.65)';
                ctx.fillRect(0, 0, ctx.canvas.width, ctx.canvas.height);

                ctx.restore();

                // draw Area
                theArea.draw();
            }
        }

        // Resets CropHost
        var resetCropHost = function() {
            if (image !== null) {
                theArea.setImage(image);
                var imageDims = [image.width, image.height],
                    imageRatio = image.width / image.height,
                    canvasDims = imageDims;

                if (canvasDims[0] > maxCanvasDims[0]) {
                    canvasDims[0] = maxCanvasDims[0];
                    canvasDims[1] = canvasDims[0] / imageRatio;
                } else if (canvasDims[0] < minCanvasDims[0]) {
                    canvasDims[0] = minCanvasDims[0];
                    canvasDims[1] = canvasDims[0] / imageRatio;
                }
                if (canvasDims[1] > maxCanvasDims[1]) {
                    canvasDims[1] = maxCanvasDims[1];
                    canvasDims[0] = canvasDims[1] * imageRatio;
                } else if (canvasDims[1] < minCanvasDims[1]) {
                    canvasDims[1] = minCanvasDims[1];
                    canvasDims[0] = canvasDims[1] * imageRatio;
                }
                elCanvas.prop('width', canvasDims[0]).prop('height', canvasDims[1]).css({
                    'margin-left': -canvasDims[0] / 2 + 'px',
                    'margin-top': -canvasDims[1] / 2 + 'px'
                });

                var cw = ctx.canvas.width;
                var ch = ctx.canvas.height;

                var areaType = self.getAreaType();
                // enforce 1:1 aspect ratio for square-like selections
                if ((areaType === 'circle') || (areaType === 'square')) {
                    if(ch < cw) cw = ch;
                    else ch = cw;
                }else if(areaType === 'rectangle' && isAspectRatio){
                  var aspectRatio = theArea.getAspect(); // use `aspectRatio` instead of `resImgSize` dimensions bc `resImgSize` can be 'selection' string
                    if(cw/ch > aspectRatio){
                        cw = aspectRatio * ch;
                    }else{
                        ch = aspectRatio * cw;
                    }
                }

                if(initMax){
                    theArea.setSize({
                        w: cw,
                        h: ch
                    });
                }else if(undefined !== theArea.getInitSize() ) {
                    theArea.setSize({
                        w: Math.min(theArea.getInitSize().w, cw / 2),
                        h: Math.min(theArea.getInitSize().h, ch / 2)
                    });
                } else {
                    theArea.setSize({
                        w: Math.min(200, cw / 2),
                        h: Math.min(200, ch / 2)
                    });
                }

                //@todo: set top left corner point
                theArea.setCenterPoint({
                    x: ctx.canvas.width / 2,
                    y: ctx.canvas.height / 2
                });

            } else {
                elCanvas.prop('width', 0).prop('height', 0).css({
                    'margin-top': 0
                });
            }

            drawScene();
        };

        var getChangedTouches = function(event) {
            if (angular.isDefined(event.changedTouches)) {
                return event.changedTouches;
            } else {
                return event.originalEvent.changedTouches;
            }
        };

        var onMouseMove = function(e) {
            if (image !== null) {
                var offset = getElementOffset(ctx.canvas),
                    pageX, pageY;
                if (e.type === 'touchmove') {
                    pageX = getChangedTouches(e)[0].pageX;
                    pageY = getChangedTouches(e)[0].pageY;
                } else {
                    pageX = e.pageX;
                    pageY = e.pageY;
                }
                theArea.processMouseMove(pageX - offset.left, pageY - offset.top);
                drawScene();
            }
        };

        var onMouseDown = function(e) {
            e.preventDefault();
            e.stopPropagation();
            if (image !== null) {
                var offset = getElementOffset(ctx.canvas),
                    pageX, pageY;
                if (e.type === 'touchstart') {
                    pageX = getChangedTouches(e)[0].pageX;
                    pageY = getChangedTouches(e)[0].pageY;
                } else {
                    pageX = e.pageX;
                    pageY = e.pageY;
                }
                theArea.processMouseDown(pageX - offset.left, pageY - offset.top);
                drawScene();
            }
        };

        var onMouseUp = function(e) {
            if (image !== null) {
                var offset = getElementOffset(ctx.canvas),
                    pageX, pageY;
                if (e.type === 'touchend') {
                    pageX = getChangedTouches(e)[0].pageX;
                    pageY = getChangedTouches(e)[0].pageY;
                } else {
                    pageX = e.pageX;
                    pageY = e.pageY;
                }
                theArea.processMouseUp(pageX - offset.left, pageY - offset.top);
                drawScene();
            }
        };

        var renderImageToDataURL = function(getResultImageSize){
            var temp_ctx, temp_canvas,
                ris = getResultImageSize,
                center = theArea.getCenterPoint(),
                retObj = {
                    dataURI: null,
                    imageData: null
                };
            temp_canvas = angular.element('<canvas></canvas>')[0];
            temp_ctx = temp_canvas.getContext('2d');
            temp_canvas.width = ris.w;
            temp_canvas.height = ris.h;
            if (image !== null) {
                var x = (center.x - theArea.getSize().w / 2) * (image.width / ctx.canvas.width),
                    y = (center.y - theArea.getSize().h / 2) * (image.height / ctx.canvas.height),
                    areaWidth = theArea.getSize().w * (image.width / ctx.canvas.width),
                    areaHeight = theArea.getSize().h * (image.height / ctx.canvas.height);

                if (forceAspectRatio) {
                    temp_ctx.drawImage(image, x, y,
                        areaWidth,
                        areaHeight,
                        0,
                        0,
                        ris.w,
                        ris.h);
                } else {
                    var aspectRatio = areaWidth / areaHeight;
                    var resultHeight, resultWidth;

                    if (aspectRatio > 1) {
                        resultWidth = ris.w;
                        resultHeight = resultWidth / aspectRatio;
                    } else {
                        resultHeight = ris.h;
                        resultWidth = resultHeight * aspectRatio;
                    }

                    temp_ctx.drawImage(image,
                        x,
                        y,
                        areaWidth,
                        areaHeight,
                        0,
                        0,
                        Math.round(resultWidth),
                        Math.round(resultHeight));
                }

                if (resImgQuality !== null) {
                    retObj.dataURI = temp_canvas.toDataURL(resImgFormat, resImgQuality);
                } else {
                    retObj.dataURI = temp_canvas.toDataURL(resImgFormat);
                }
            }
            return retObj;
        };

        this.getResultImage = function() {
            if(resImgSizeArray.length==0){
                return renderImageToDataURL(this.getResultImageSize());
            }else{
                var arrayResultImages=[];
                for (var i = 0; i < resImgSizeArray.length; i++) {
                    arrayResultImages.push({
                        dataURI:renderImageToDataURL(resImgSizeArray[i]).dataURI,
                        w:resImgSizeArray[i].w,
                        h:resImgSizeArray[i].h
                    });
                };
                return arrayResultImages;
            }
        };

        this.getResultImageDataBlob = function() {
            var temp_ctx, temp_canvas,
                center = theArea.getCenterPoint(),
                ris = this.getResultImageSize(),
                _p = $q.defer();
            temp_canvas = angular.element('<canvas></canvas>')[0];
            temp_ctx = temp_canvas.getContext('2d');
            temp_canvas.width = ris.w;
            temp_canvas.height = ris.h;
            if (image !== null) {
                var x = (center.x - theArea.getSize().w / 2) * (image.width / ctx.canvas.width),
                    y = (center.y - theArea.getSize().h / 2) * (image.height / ctx.canvas.height),
                    areaWidth = theArea.getSize().w * (image.width / ctx.canvas.width),
                    areaHeight = theArea.getSize().h * (image.height / ctx.canvas.height);

                if (forceAspectRatio) {
                    temp_ctx.drawImage(image, x, y,
                        areaWidth,
                        areaHeight,
                        0,
                        0,
                        ris.w,
                        ris.h);
                } else {
                    var aspectRatio = areaWidth / areaHeight;
                    var resultHeight, resultWidth;

                    if (aspectRatio > 1) {
                        resultWidth = ris.w;
                        resultHeight = resultWidth / aspectRatio;
                    } else {
                        resultHeight = ris.h;
                        resultWidth = resultHeight * aspectRatio;
                    }

                    temp_ctx.drawImage(image,
                        x,
                        y,
                        areaWidth,
                        areaHeight,
                        0,
                        0,
                        Math.round(resultWidth),
                        Math.round(resultHeight));
                }
            }
            temp_canvas.toBlob(function(blob) {
                _p.resolve(blob);
            }, resImgFormat);
            return _p.promise;
        };

        this.getAreaCoords = function() {
            return theArea.getSize()
        };

        this.getArea = function() {
          return theArea;
        }

        this.setNewImageSource = function(imageSource) {
            image = null;
            resetCropHost();
            events.trigger('image-updated');
            if (!!imageSource) {
                var newImage = new Image();
                newImage.onload = function() {
                    events.trigger('load-done');

                    image = newImage;
                    
                    resetCropHost();
                    events.trigger('image-updated');
                };
                newImage.onerror = function() {
                    events.trigger('load-error');
                };
                events.trigger('load-start');
                if (imageSource instanceof window.Blob) {
                    newImage.src = URL.createObjectURL(imageSource);
                } else {
                    if (imageSource.substring(0, 4).toLowerCase() === 'http') {
                      newImage.crossOrigin = 'anonymous';
                    }
                    newImage.src = imageSource;
                }
            }
        };

        this.setMaxDimensions = function(width, height) {
            maxCanvasDims = [width, height];

            if (image !== null) {
                var curWidth = ctx.canvas.width,
                    curHeight = ctx.canvas.height;

                var imageDims = [image.width, image.height],
                    imageRatio = image.width / image.height,
                    canvasDims = imageDims;

                if (canvasDims[0] > maxCanvasDims[0]) {
                    canvasDims[0] = maxCanvasDims[0];
                    canvasDims[1] = canvasDims[0] / imageRatio;
                } else if (canvasDims[0] < minCanvasDims[0]) {
                    canvasDims[0] = minCanvasDims[0];
                    canvasDims[1] = canvasDims[0] / imageRatio;
                }
                if (canvasDims[1] > maxCanvasDims[1]) {
                    canvasDims[1] = maxCanvasDims[1];
                    canvasDims[0] = canvasDims[1] * imageRatio;
                } else if (canvasDims[1] < minCanvasDims[1]) {
                    canvasDims[1] = minCanvasDims[1];
                    canvasDims[0] = canvasDims[1] * imageRatio;
                }
                elCanvas.prop('width', canvasDims[0]).prop('height', canvasDims[1]).css({
                    'margin-left': -canvasDims[0] / 2 + 'px',
                    'margin-top': -canvasDims[1] / 2 + 'px'
                });

                var ratioNewCurWidth = ctx.canvas.width / curWidth,
                    ratioNewCurHeight = ctx.canvas.height / curHeight,
                    ratioMin = Math.min(ratioNewCurWidth, ratioNewCurHeight);

                //TODO: use top left corner point
                theArea.setSize({
                    w: theArea.getSize().w * ratioMin,
                    h: theArea.getSize().h * ratioMin
                });
                var center = theArea.getCenterPoint();
                theArea.setCenterPoint({
                    x: center.x * ratioNewCurWidth,
                    y: center.y * ratioNewCurHeight
                });

            } else {
                elCanvas.prop('width', 0).prop('height', 0).css({
                    'margin-top': 0
                });
            }

            drawScene();

        };

        this.setAreaMinSize = function(size) {
            if (angular.isUndefined(size)) {
                return;
            } else if (typeof size == 'number' || typeof size == 'string') {
                size = {
                    w: parseInt(parseInt(size), 10),
                    h: parseInt(parseInt(size), 10)
                };
            } else {
                size = {
                    w: parseInt(size.w, 10),
                    h: parseInt(size.h, 10)
                };
            }
            if (!isNaN(size.w) && !isNaN(size.h)) {
                theArea.setMinSize(size);
                drawScene();
            }
        };

        this.setAreaMinRelativeSize = function(size) {
            if (image !== null) {
              var canvasSize = theArea.getCanvasSize();
              if (angular.isUndefined(size)) {
                  return;
              } else if(typeof size == 'number' || typeof size == 'string') {
                  areaMinRelativeSize = {
                      w: size,
                      h: size
                  };
                  size = {
                      w: canvasSize.w/(image.width/parseInt(parseInt(size), 10)),
                      h: canvasSize.h/(image.height/parseInt(parseInt(size), 10))
                  };
              } else{
                  areaMinRelativeSize = size;
                  size = {
                      w: canvasSize.w/(image.width/parseInt(parseInt(size.w), 10)),
                      h: canvasSize.h/(image.height/parseInt(parseInt(size.h), 10))
                  };
              }
              if (!isNaN(size.w) && !isNaN(size.h)) {
                  theArea.setMinSize(size);
                  drawScene();
              }
            }
        };

        this.setAreaInitSize = function(size) {
            if (angular.isUndefined(size)) {
                return;
            }else if(typeof size == 'number' || typeof size == 'string'){
                size = {
                    w: parseInt(parseInt(size), 10),
                    h: parseInt(parseInt(size), 10)
                };
            }else{
                size = {
                    w: parseInt(size.w, 10),
                    h: parseInt(size.h, 10)
                };
            }
            if (!isNaN(size.w) && !isNaN(size.h)) {
                theArea.setInitSize(size);
                drawScene();
            }
        };

        this.getResultImageSize = function() {
            if (resImgSize == "selection") {
                return theArea.getSize();
            }else if(resImgSize == "max") {
                 // We maximize the rendered size
                var zoom = 1;
                if (image && ctx && ctx.canvas) {
                    zoom = image.width / ctx.canvas.width;
                }
                var size = {
                    w: zoom * theArea.getSize().w,
                    h: zoom * theArea.getSize().h
                };

                if (areaMinRelativeSize) {
                  if (size.w < areaMinRelativeSize.w) {
                    size.w = areaMinRelativeSize.w;
                  }
                  if (size.h < areaMinRelativeSize.h) {
                    size.h = areaMinRelativeSize.h;
                  }
                }

                return size;
            }

            return resImgSize;
        };

        this.setResultImageSize = function(size) {
            if(angular.isArray(size)){
                resImgSizeArray=size.slice();
                size = {
                    w: parseInt(size[0].w, 10),
                    h: parseInt(size[0].h, 10)
                };
                return;
            }
            if (angular.isUndefined(size)) {
                return;
            }
            //allow setting of size to "selection" for mirroring selection's dimensions
            if (angular.isString(size)) {
                resImgSize = size;
                return;
            }
            //allow scalar values for square-like selection shapes
            if (angular.isNumber(size)) {
                size = parseInt(size, 10);
                size = {
                    w: size,
                    h: size
                };
            }
            size = {
                w: parseInt(size.w, 10),
                h: parseInt(size.h, 10)
            };
            if (!isNaN(size.w) && !isNaN(size.h)) {
                resImgSize = size;
                drawScene();
            }
        };

        this.setResultImageFormat = function(format) {
            resImgFormat = format;
        };

        this.setResultImageQuality = function(quality) {
            quality = parseFloat(quality);
            if (!isNaN(quality) && quality >= 0 && quality <= 1) {
                resImgQuality = quality;
            }
        };

        // returns a string of the selection area's type
        this.getAreaType = function() {
            return theArea.getType();
        }

        this.setAreaType = function(type) {
            var center = theArea.getCenterPoint();
            var curSize = theArea.getSize(),
                curMinSize = theArea.getMinSize(),
                curX = center.x,
                curY = center.y;

            var AreaClass = CropAreaRectangle;
            
            theArea = new AreaClass(ctx, events);
            theArea.setMinSize(curMinSize);
            theArea.setSize(curSize);
            theArea.setCenterPoint({
                x: curX,
                y: curY
            });

            // resetCropHost();
            if (image !== null) {
                theArea.setImage(image);
            }

            drawScene();
        };

        this.setAspect = function(aspect) {
            // isAspectRatio=true;
            theArea.setAspect(aspect);
            // var minSize = theArea.getMinSize();
            // minSize.w=minSize.h*aspect;
            // theArea.setMinSize(minSize);
            // var size = theArea.getSize();
            // size.w=size.h*aspect;
            // theArea.setSize(size);
        };

        /* Life Cycle begins */

        // Init Context var
        ctx = elCanvas[0].getContext('2d');

        // Init CropArea
        theArea = new CropAreaRectangle(ctx, events);

        // Init Mouse Event Listeners
        $document.on('mousemove', onMouseMove);
        elCanvas.on('mousedown', onMouseDown);
        $document.on('mouseup', onMouseUp);

        // Init Touch Event Listeners
        $document.on('touchmove', onMouseMove);
        elCanvas.on('touchstart', onMouseDown);
        $document.on('touchend', onMouseUp);

        // CropHost Destructor
        this.destroy = function() {
            $document.off('mousemove', onMouseMove);
            elCanvas.off('mousedown', onMouseDown);
            $document.off('mouseup', onMouseMove);

            $document.off('touchmove', onMouseMove);
            elCanvas.off('touchstart', onMouseDown);
            $document.off('touchend', onMouseMove);

            elCanvas.remove();
        };
    };
}]);

crop.factory('cropPubSub', [function() {
    return function() {
        var events = {};
        // Subscribe
        this.on = function(names, handler) {
            names.split(' ').forEach(function(name) {
                if (!events[name]) {
                    events[name] = [];
                }
                events[name].push(handler);
            });
            return this;
        };
        // Publish
        this.trigger = function(name, args) {
            angular.forEach(events[name], function(handler) {
                handler.call(null, args);
            });
            return this;
        };
    };
}]);

crop.directive('imgCrop', ['$timeout', 'cropHost', 'cropPubSub', function ($timeout, CropHost, CropPubSub) {
    return {
        restrict: 'E',
        scope: {
            image: '=',
            resultImage: '=',
            resultArrayImage: '=?',
            resultBlob: '=?',
            urlBlob: '=?',
            chargement: '=?',
            cropject: '=?',

            changeOnFly: '=?',
            liveView: '=?',
            initMaxArea: '=?',
            areaCoords: '=?',
            areaType: '@',
            areaMinSize: '=?',
            areaInitSize: '=?',
            areaMinRelativeSize: '=?',
            resultImageSize: '=?',
            resultImageFormat: '=?',
            resultImageQuality: '=?',

            aspectRatio: '=?',
            allowCropResizeOnCorners: '=?',

            dominantColor: '=?',
            paletteColor: '=?',
            paletteColorLength: '=?',

            onChange: '&',
            onLoadBegin: '&',
            onLoadDone: '&',
            onLoadError: '&'
        },
        template: '<canvas></canvas>',
        controller: ['$scope', function ($scope) {
            $scope.events = new CropPubSub();
        }],
        link: function (scope, element) {

            if (scope.liveView && typeof scope.liveView.block == 'boolean') {
                scope.liveView.render = function (callback) {
                    updateResultImage(scope, true, callback);
                }
            } else scope.liveView = {block: false};

            // Init Events Manager
            var events = scope.events;

            // Init Crop Host
            var cropHost = new CropHost(element.find('canvas'), {}, events);

            // Store Result Image to check if it's changed
            var storedResultImage;

            var updateResultImage = function (scope, force, callback) {
                if (scope.image !== '' && (!scope.liveView.block || force)) {
                    var resultImageObj = cropHost.getResultImage();
                    if (angular.isArray(resultImageObj)) {
                        resultImage = resultImageObj[0].dataURI;
                        scope.resultArrayImage = resultImageObj;
                        console.log(scope.resultArrayImage);
                    } else var resultImage = resultImageObj.dataURI;

                    var urlCreator = window.URL || window.webkitURL;
                    if (storedResultImage !== resultImage) {
                        storedResultImage = resultImage;
                        scope.resultImage = resultImage;
                        if (scope.liveView.callback) scope.liveView.callback(resultImage);
                        if (callback) callback(resultImage);
                        cropHost.getResultImageDataBlob().then(function (blob) {
                            scope.resultBlob = blob;
                            scope.urlBlob = urlCreator.createObjectURL(blob);
                        });

                        updateAreaCoords(scope);
                        scope.onChange({
                            $dataURI: scope.resultImage
                        });
                    }
                }
            };

            var updateAreaCoords = function (scope) {
                var areaCoords = cropHost.getAreaCoords();
                scope.areaCoords = areaCoords;
            };

            var updateCropject = function (scope) {
                var areaCoords = cropHost.getAreaCoords();

                var dimRatio = {
                  x: cropHost.getArea().getImage().width / cropHost.getArea().getCanvasSize().w,
                  y: cropHost.getArea().getImage().height / cropHost.getArea().getCanvasSize().h
                };

                scope.cropject = {
                    areaCoords: areaCoords,
                    cropWidth: areaCoords.w,
                    cropHeight: areaCoords.h,
                    cropTop: areaCoords.y,
                    cropLeft: areaCoords.x,
                    cropImageWidth: Math.round(areaCoords.w * dimRatio.x),
                    cropImageHeight: Math.round(areaCoords.h * dimRatio.y),
                    cropImageTop: Math.round(areaCoords.y * dimRatio.y),
                    cropImageLeft: Math.round(areaCoords.x * dimRatio.x)
                };
            };

            // Wrapper to safely exec functions within $apply on a running $digest cycle
            var fnSafeApply = function (fn) {
                return function () {
                    $timeout(function () {
                        scope.$apply(function (scope) {
                            fn(scope);
                        });
                    });
                };
            };

            if (scope.chargement == null) scope.chargement = 'Chargement';
            var displayLoading = function () {
                element.append('<div class="loading"><span>' + scope.chargement + '...</span></div>')
            };

            // Setup CropHost Event Handlers
            events
                .on('load-start', fnSafeApply(function (scope) {
                    scope.onLoadBegin({});
                }))
                .on('load-done', fnSafeApply(function (scope) {
                    angular.element(element.children()[element.children().length - 1]).remove();
                    scope.onLoadDone({});
                }))
                .on('load-error', fnSafeApply(function (scope) {
                    scope.onLoadError({});
                }))
                .on('area-move area-resize', fnSafeApply(function (scope) {
                    if (!!scope.changeOnFly) {
                        updateResultImage(scope);
                    }
                    updateCropject(scope);
                }))
                .on('area-move-end area-resize-end image-updated', fnSafeApply(function (scope) {
                    updateResultImage(scope);
                    updateCropject(scope);
                }))
                .on('image-updated', fnSafeApply(function(scope) {
                    cropHost.setAreaMinRelativeSize(scope.areaMinRelativeSize);
                }));

            // Sync CropHost with Directive's options
            scope.$watch('image', function (newVal) {
                if (newVal) {
                    displayLoading();
                }
                $timeout(function () {
                    cropHost.setInitMax(scope.initMaxArea);
                    cropHost.setNewImageSource(scope.image);
                }, 100);
            });
            scope.$watch('areaType', function () {
                cropHost.setAreaType(scope.areaType);
                updateResultImage(scope);
            });
            scope.$watch('areaMinSize', function () {
                cropHost.setAreaMinSize(scope.areaMinSize);
                updateResultImage(scope);
            });
            scope.$watch('areaMinRelativeSize', function () {
                if (scope.image !== '') {
                    cropHost.setAreaMinRelativeSize(scope.areaMinRelativeSize);
                    updateResultImage(scope);
                }
            });
            scope.$watch('areaInitSize', function () {
                cropHost.setAreaInitSize(scope.areaInitSize);
                updateResultImage(scope);
            });
            scope.$watch('resultImageFormat', function () {
                cropHost.setResultImageFormat(scope.resultImageFormat);
                updateResultImage(scope);
            });
            scope.$watch('resultImageQuality', function () {
                cropHost.setResultImageQuality(scope.resultImageQuality);
                updateResultImage(scope);
            });
            scope.$watch('resultImageSize', function () {
                cropHost.setResultImageSize(scope.resultImageSize);
                updateResultImage(scope);
            });
            scope.$watch('aspectRatio', function () {
                if (typeof scope.aspectRatio == 'string' && scope.aspectRatio != '') {
                    scope.aspectRatio = parseInt(scope.aspectRatio);
                }
                cropHost.setAspect(scope.aspectRatio);
            });
            scope.$watch('allowCropResizeOnCorners', function () {
                if (scope.allowCropResizeOnCorners) cropHost.setAllowCropResizeOnCorners(scope.allowCropResizeOnCorners);
            });

            // Update CropHost dimensions when the directive element is resized
            scope.$watch(
                function () {
                    return [element[0].clientWidth, element[0].clientHeight];
                },
                function (value) {
                    cropHost.setMaxDimensions(value[0], value[1]);
                    updateResultImage(scope);
                },
                true
            );

            // Destroy CropHost Instance when the directive is destroying
            scope.$on('$destroy', function () {
                cropHost.destroy();
            });
        }
    };
}]);

(function(view) {
    "use strict";
    var
        Uint8Array = view.Uint8Array,
        HTMLCanvasElement = view.HTMLCanvasElement,
        canvas_proto = HTMLCanvasElement && HTMLCanvasElement.prototype,
        is_base64_regex = /\s*;\s*base64\s*(?:;|$)/i,
        to_data_url = "toDataURL",
        base64_ranks, decode_base64 = function(base64) {
            var
                len = base64.length,
                buffer = new Uint8Array(len / 4 * 3 | 0),
                i = 0,
                outptr = 0,
                last = [0, 0],
                state = 0,
                save = 0,
                rank, code, undef;
            while (len--) {
                code = base64.charCodeAt(i++);
                rank = base64_ranks[code - 43];
                if (rank !== 255 && rank !== undef) {
                    last[1] = last[0];
                    last[0] = code;
                    save = (save << 6) | rank;
                    state++;
                    if (state === 4) {
                        buffer[outptr++] = save >>> 16;
                        if (last[1] !== 61 /* padding character */ ) {
                            buffer[outptr++] = save >>> 8;
                        }
                        if (last[0] !== 61 /* padding character */ ) {
                            buffer[outptr++] = save;
                        }
                        state = 0;
                    }
                }
            }
            // 2/3 chance there's going to be some null bytes at the end, but that
            // doesn't really matter with most image formats.
            // If it somehow matters for you, truncate the buffer up outptr.
            return buffer;
        };
    if (Uint8Array) {
        base64_ranks = new Uint8Array([
            62, -1, -1, -1, 63, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -1, -1, -1, 0, -1, -1, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -1, -1, -1, -1, -1, -1, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51
        ]);
    }
    if (HTMLCanvasElement && !canvas_proto.toBlob) {
        canvas_proto.toBlob = function(callback, type /*, ...args*/ ) {
            if (!type) {
                type = "image/png";
            }
            if (this.mozGetAsFile) {
                callback(this.mozGetAsFile("canvas", type));
                return;
            }
            if (this.msToBlob && /^\s*image\/png\s*(?:$|;)/i.test(type)) {
                callback(this.msToBlob());
                return;
            }

            var
                args = Array.prototype.slice.call(arguments, 1),
                dataURI = this[to_data_url].apply(this, args),
                header_end = dataURI.indexOf(","),
                data = dataURI.substring(header_end + 1),
                is_base64 = is_base64_regex.test(dataURI.substring(0, header_end)),
                blob;
            if (Blob.fake) {
                // no reason to decode a data: URI that's just going to become a data URI again
                blob = new Blob
                if (is_base64) {
                    blob.encoding = "base64";
                } else {
                    blob.encoding = "URI";
                }
                blob.data = data;
                blob.size = data.length;
            } else if (Uint8Array) {
                if (is_base64) {
                    blob = new Blob([decode_base64(data)], {
                        type: type
                    });
                } else {
                    blob = new Blob([decodeURIComponent(data)], {
                        type: type
                    });
                }
            }
            if (typeof callback !== 'undefined') {
                callback(blob);
            }
        };

        if (canvas_proto.toDataURLHD) {
            canvas_proto.toBlobHD = function() {
                to_data_url = "toDataURLHD";
                var blob = this.toBlob();
                to_data_url = "toDataURL";
                return blob;
            }
        } else {
            canvas_proto.toBlobHD = canvas_proto.toBlob;
        }
    }
}(typeof self !== "undefined" && self || typeof window !== "undefined" && window || this.content || this));

}());

    