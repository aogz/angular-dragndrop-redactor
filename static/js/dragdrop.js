var currentIdNum = 0;

function generateUniqueId() {
    if (!currentIdNum) {
        currentIdNum = 0;
    }

    currentIdNum += 1;
    return 'draggable_id_' + currentIdNum;
}

main_app.directive('html5Drag', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var dragData = scope.$eval(attrs.dragData),
            onDragStart = scope.$eval(attrs.onDragStart),
            onDrag = scope.$eval(attrs.onDrag),
            onDragEnd = scope.$eval(attrs.onDragEnd);

                // Assign a unique id to the element if it has no id.
                if (!element.attr('id')) {
                    element.attr('id', generateUniqueId());
                }

                // Add attributes to the element to make it draggable.
                element.attr('draggable', true);
                element.data('dragData', dragData);

                element.on('dragstart', function (event) {
                    event.originalEvent.dataTransfer.setData('text/plain', element.attr('id'));
                    angular.element(document.getElementsByClassName('dragging')).removeClass('dragging');
                    element.addClass('dragging');

                    if (angular.isFunction(onDragStart)) {
                        onDragStart(event, element, element.data('dragData'));
                    }
                });

                element.on('drag', function (event) {
                    if (angular.isFunction(onDrag)) {
                        onDrag(event, element, element.data('dragData'));
                    }
                });

                element.on('dragend', function (event) {
                    element.removeClass('dragging');
                    // element.attr('draggable', false);
                    if (angular.isFunction(onDragEnd)) {
                        onDragEnd(event, element, element.data('dragData'));
                    }
                });
            }
        };
    });

main_app.directive('html5Drop', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var onDragEnter = scope.$eval(attrs.onDragEnter),
            onDragOver  = scope.$eval(attrs.onDragOver),
            onDragLeave = scope.$eval(attrs.onDragLeave),
            onDrop      = scope.$eval(attrs.onDrop);

            element.on('dragenter', function (event) {
                event.preventDefault();

                if (angular.isFunction(onDragEnter)) {
                    var draggedNode = document.getElementsByClassName('dragging')[0],
                    draggedEl = angular.element(draggedNode);
                    onDragEnter(event, element, draggedEl, draggedEl.data('dragData'));
                }
            });

            element.on('dragover', function (event) {
                event.preventDefault();

                if (angular.isFunction(onDragOver)) {
                    var draggedNode = document.getElementsByClassName('dragging')[0],
                    draggedEl = angular.element(draggedNode);
                    onDragOver(event, element, draggedEl, draggedEl.data('dragData'));
                }
            });

            element.on('dragleave', function (event) {
                if (angular.isFunction(onDragLeave)) {
                    var draggedNode = document.getElementsByClassName('dragging')[0],
                    draggedEl = angular.element(draggedNode);
                    onDragLeave(event, element, draggedEl, draggedEl.data('dragData'));
                }
            });

            element.on('drop', function (event) {
                event.preventDefault();

                if (angular.isFunction(onDrop)) {
                    var dragElementId = event.originalEvent.dataTransfer.getData('text/plain'),
                    draggedEl = angular.element(document.getElementById(dragElementId));
                    onDrop(event, element, draggedEl, draggedEl.data('dragData'));
                }
            });
        }
    };
});