main_app.controller('briefController', function ($scope, $compile, $timeout) {
    $scope.tinymceOptions = {
        menu: {},
            // skin: "../../../css/vendor",
        }

        $scope.myCroppedImage = '';
        $scope.myImage = '';

        $scope.rectangleWidth = 100;
        $scope.rectangleHeight = 100;

        $scope.cropper = {
          cropWidth: $scope.rectangleWidth,
          cropHeight: $scope.rectangleHeight
      };

      var handleFileSelect = function(evt) {
          var file = evt.currentTarget.files[0];
          var reader = new FileReader();
          reader.onload = function(evt) {
            $scope.$apply(function($scope) {
              $scope.editable_image.src = evt.target.result;
              if (!$scope.images[$scope.editable_image.id]) $scope.images[$scope.editable_image.id] = {}
                  $scope.images[$scope.editable_image.id].original = evt.target.result;
          });
        };
        reader.readAsDataURL(file);
    };
    angular.element(document.querySelector('#fileInput')).on('change', handleFileSelect);
    $scope.templates = [
    {url: 'brief-templates/basic-brief.html', title: 'basic brief', img: ''},
    {url: 'brief-templates/row-brief.html', title: 'row brief', img: ''},
    {url: 'brief-templates/table-brief.html', title: 'table brief', img: ''}
    ]

    $scope.showed_controls = {}

    $scope.preview = false;

    $scope.drop = function () {
        return function (event, element, draggable) {
            if (draggable[0] == undefined) return false
                var attrs = draggable[0].attributes;
            if (!attrs.hasOwnProperty('html5-drag')) return false

                // draggable.attr('draggable', 'false');
            if (!attrs.hasOwnProperty('data-copy')){
                var unique_id = generateUniqueId();
                draggable = angular.copy(draggable);
                draggable.attr('id', unique_id);
                draggable.attr('data-copy', 'true');
            } else {
                var unique_id = attrs.id.value;
            }
            if (draggable[0].querySelector('.wrapper-' + unique_id) || !element[0].innerHTML.length) {
                angular.element(draggable[0].querySelector('.wrapper-' + unique_id)).remove()
            }
            var wrapper = angular.element('<div class="wrapper-' + unique_id + '"></div>')
            var control = angular.element('<div class="control"></div>');
            var value = angular.element('<div class="result"></div>');
            if (attrs.hasOwnProperty('text-element')){        
                var textarea = angular.element('<textarea class="control" ui-tinymce></textarea>')
                value.attr('ng-bind-html', unique_id + ' | sanitize')
                textarea.attr('ng-model', unique_id);    
                control.append(textarea);
                control.attr('ng-show', "!showed_controls['control_" + unique_id + "']")
                control.attr('ng-hide', "preview")
                value.attr('ng-show', "showed_controls['control_" + unique_id + "']")
            } else if (attrs.hasOwnProperty('image-element')) {
                var image_preview = angular.element('<img ng-src="{{ images.' + unique_id + '.cropped }}" id="image_' + unique_id + '"/>')
                value.addClass('center-align')
                value.append(image_preview);
                $scope.editable_image = {}
            } else if (attrs.hasOwnProperty('calculation-element')) {

            } else if (attrs.hasOwnProperty('aircraft-element')) {
                // Open modal window
                // Select aircraft, save to aircraft = {}
                // on click save, create cards with images and info
                // ?? Remove parent div aka card

            } else {
                console.log('Unknown element')
            }

            // control.attr('ng-show', 'show_toolbars')
            // value.attr('ng-show', '!show_toolbars')
            wrapper.append(control);
            wrapper.append(value);
            draggable.append(wrapper);
            
            if (attrs.hasOwnProperty('image-element') && (!attrs.hasOwnProperty('data-copy'))) {
                $scope.openEdit(unique_id);
            } 
            
            element.append ($compile(draggable)($scope));
            }

        };

    $scope.calcAspect = function(id) {
        console.log('boom')
        if ($scope.aspectBool) $scope.images[id].aspect = $scope.images[id].cropper.cropWidth / $scope.images[id].cropper.cropHeight;
        else  $scope.images[id].aspect = 'no'
    }

    $scope.selectTemplate = function(template){
        $scope.url = template.url;
    }

    $scope.generateBrief = function() {
        $scope.preview = !$scope.preview;
        for (var i = currentIdNum; i > 0; i--) {
            $scope.showed_controls['control_draggable_id_' + currentIdNum] = !$scope.showed_controls['control_draggable_id_' + currentIdNum];          
        };
    }

    $scope.saveImageState = function() {
        $scope.images[$scope.editable_image.id] = $scope.editable_image.cropped;
    }

    $scope.compileImage = function (unique_id) {
        $compile(angular.element(document.querySelector('#image_' + unique_id))[0])($scope)
    }
});
