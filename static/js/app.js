var main_app = angular.module('mainApp', ['ui.tinymce', 'ngImgCrop'])

main_app.filter("sanitize", ['$sce', function($sce) {
  return function(htmlCode){
    return $sce.trustAsHtml(htmlCode);
}
}]);

main_app.directive('imageModal', function($compile) {
    return {
      restrict: 'A',
      controller: 'briefController',
      link: function(scope, element, attrs){
        scope.openEdit = function(unique_id) {
            scope.editable_image = {
                src: scope.images[unique_id]? scope.images[unique_id].original : '',
                id: unique_id
            }

            $(element).openModal({complete: function() { 
                // scope.compileImage(unique_id);
                scope.images[unique_id].showed = true;
            }})
        }
    }
}

});

main_app.directive("toolbar", function($compile){
    return {
        restrict: 'A',

        template: function(element, attrs) {
            var unique_id = element[0].parentNode.id;
            element.attr('id', unique_id);

            return '<a class="btn-flat tooltipped" ng-click="removeDiv(\'' + unique_id + '\')"  data-balloon="Delete element" data-balloon-pos="up"><i class="material-icons">delete</i></a>' + 
            '<a class="btn-flat right" ng-click="hideControl(\'' + unique_id + '\')" data-balloon="Toggle editing" data-balloon-pos="up"><i class="material-icons">check</i></a>'
        },
        controller: 'briefController',
        link: function (scope, element, attrs) {

            scope.removeDiv = function(unique_id){
                angular.element(document.getElementById(unique_id)).remove()
            };

            scope.hideControl = function(unique_id) {
                var control_id = 'control_' + unique_id;
                values = angular.element(document.querySelector('#' + unique_id))[0].attributes
                if (values.hasOwnProperty('image-element')){
                    scope.openEdit(unique_id);
                    scope.images[unique_id].showed = false;
                    return false
                }
                               
               scope.showed_controls[control_id] = !scope.showed_controls[control_id];
           }

       }
   }
})